#!/usr/bin/python3
# coding: utf-8

'''repair MPD playlists'''
# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
import fileinput
import logging
import os
import re
import subprocess
import sys
py3 = sys.version_info[0] > 2

# intput from python2 is evil and doesn't make any sense, explicitely
# use eval if really necessary
if not py3:
    input = raw_input


def parse_playlists(args):
    backup = type(args.inplace) is not bool and args.inplace
    files = fileinput.input(args.playlists,
                            inplace=bool(args.inplace),
                            backup=backup)

    for song in files:
        song = song.rstrip()
        logging.debug('processing song %s', song)
        if os.path.exists(song):
            print(song)
        else:
            logging.info('song missing: %s', song)
            # generate_patterns may yield duplicates, filter those
            for pattern, searchtype in generate_patterns(song):
                result = pick_results(mpd_search(pattern, searchtype), song)
                if result:
                    print(result)
                    break
            else:
                logging.warning('no match found for: %s', song)


def pick_results(results, song):
    if len(results) > 1:
        if sys.stdin.isatty():
            i = 0
            for result in results:
                print('%d: %s' % (i, result), file=sys.stderr)
                i += 1
            print('multiple matches found for %s' % song, file=sys.stderr)
            print('please pick number in the above list '
                  'or type "n" to keep searching: ',
                  file=sys.stderr, end='')
            try:
                idx = input().rstrip()
                if idx == 'n':
                    return False
                result = results[int(idx)]
            except (ValueError, IndexError) as message:
                logging.warning(message)
                return pick_results(results, song)
            logging.info('picked match: %s', results[0])
            return result
        else:
            logging.warning('multiple matches found and terminal not available'
                            ', adding all matches')
            logging.warning('pass playlist as an argument instead of '
                            'using stdin to get prompted')
            return "\n".join(results)
    elif len(results) == 1:
        logging.info('found single match: %s', results[0])
        return results[0]
    return False


def generate_patterns(song):
    '''given the song, generate possible search tokens

    this was created iteratively, so it's a bunch of patterns and
    calls, could be best implemented with a single regex, probably.

    but it's not the slow part of this program (mpc_search() is), so i
    didn't bother.
    '''
    patterns = [(song, 'filename')]
    # strip directories
    basename = os.path.basename(song)
    if basename != song:
        patterns += [(basename, 'filename')]
    # strip extension
    songname = os.path.splitext(basename)[0]
    if songname != basename:
        patterns += [(songname, 'any')]
    # remove leading track number
    res = re.match(r'(?:\d+ (?:- )?)(.*)', songname)
    if res:
        songname = res.group(1)
        patterns += [(songname, 'any')]
    # remove possible band
    res = re.match(r'(?:.*) - (.*)$', songname)
    if res:
        patterns += [(res.group(1), 'any')]
    return patterns


def mpc_search(path, searchtype='any'):
    logging.info('looking for pattern %s: %s', searchtype, path)
    try:
        results = subprocess.check_output(['mpc',
                                           'search',
                                           searchtype,
                                           path])
        results = results.decode('utf-8').splitlines()
    except subprocess.CalledProcessError as e:
        logging.exception('mpc failed: %s', e)
    if results:
        logging.debug('result found: %s', ", ".join(results))
    return results


# http://stackoverflow.com/a/6798042/1174784
class _Singleton(type):
    """ A metaclass that creates a Singleton base class when called. """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(_Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Singleton(_Singleton('SingletonMeta', (object,), {})):
    pass


class MPD_searcher(Singleton):
    def __init__(self):
        try:
            password, host = os.environ.get('MPD_HOST', 'localhost').split('@')
        except ValueError:
            host = os.environ.get('MPD_HOST', 'localhost').split('@')
            password = None

        port = os.environ.get('MPD_PORT', 6600)
        logging.info('connecting to MPD daemon %s:%d', host, port)
        self.client = musicpd.MPDClient()
        self.client.connect(host, port)
        self.client.password(password)

    def __del__(self):
        logging.info('disconnecting from MPD daemon')
        self.client.close()
        self.client.disconnect()


def musicpd_search(path, searchtype='any'):
    res = MPD_searcher().client.search(searchtype, path)
    return [f['file'] for f in res]


try:
    import musicpd
except ImportError:
    mpd_search = mpc_search
else:
    mpd_search = musicpd_search


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__,
                                     epilog='''Iterate over the given
                                     PLAYLISTS, detect missing files
                                     and try to find them using
                                     mpc. This should be able to
                                     update your playlists to follow
                                     file moves easily. Note that this
                                     will be much faster if
                                     python3-musicpd is installed,
                                     otherwise the `mpc` client is
                                     used.''')
    parser.add_argument('--verbose', '-v', dest='log_level',
                        action='store_const', const='info', default='warning')
    parser.add_argument('--debug', '-d', dest='log_level',
                        action='store_const', const='debug', default='warning')
    parser.add_argument('--inplace', '-i', nargs='?', const=True, metavar='BACKUP',
                        help='''fix files in place with optional BACKUP suffix
                        (default: print to stdout)''')
    parser.add_argument('playlists', nargs='*',
                        help='''list of playlists to inspect''')
    args = parser.parse_args()

    logging.basicConfig(format='%(message)s', level=args.log_level.upper())

    parse_playlists(args)
