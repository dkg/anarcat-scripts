#!/usr/bin/python3

'''"Division du vote" in PQ/QS results in 2018 Quebec general election'''

__epilog__ = '''How to fetch the list of JSON files from the DGEQ:

  curl -s https://resultats.dgeq.org/resultatsSommaires.fr.html \
   | grep 'option value=' | sed 's/.*value="//;s/".*//' | sort -u \
   | while read circ; do
       echo https://www.dgeq.org/$circ.json;
     done > url-list.txt ;
  wget --input-file=url-list.txt
'''


import argparse
import json
import sys

parser = argparse.ArgumentParser(description=__doc__, epilog=__epilog__)
parser.add_argument('json', nargs='+', help='JSON files from the DGEQ')
args = parser.parse_args()

counties = {}

lost_pq_txt = []
lost_qs_txt = []
ties_txt = []
lost_pq = []
lost_qs = []
ties = []

for path in args.json:
    c = path.split('.')[0]
    with open(path) as pathfp:
        data = json.load(pathfp)
        counties[int(c)] = data
        total_qs_pq = 0
        winner = 0
        party = {}
        for candidate in data['candidats']:
            if candidate['nbVoteTotal'] > winner:
                winner = candidate['nbVoteTotal']
            party[candidate['abreviationPartiPolitique']] = candidate['nbVoteTotal']
        total_qs_pq = party['Q.S.'] + party['P.Q.']
        if total_qs_pq > winner:
            # special case: QS or PQ won
            # we assert there are no ties here
            if winner == party['P.Q.']:
                if data['candidats'][1]['abreviationPartiPolitique'] == 'Q.S.':
                    lost_qs.append(data['numeroCirconscription'])
                    lost_qs_txt.append(data['nomCirconscription'])
            elif winner == party['Q.S.']:
                if data['candidats'][1]['abreviationPartiPolitique'] == 'P.Q.':
                    lost_pq.append(data['numeroCirconscription'])
                    lost_pq_txt.append(data['nomCirconscription'])
            else:
                # biased towards PQ in case of tie
                if party['Q.S.'] > party['P.Q.']:
                    print("QS would have won with PQ's %d votes by %d votes in %s"
                          % (party['P.Q.'], total_qs_pq - winner,
                             data['nomCirconscription']))
                    lost_qs.append(data['numeroCirconscription'])
                    lost_qs_txt.append(data['nomCirconscription'])
                if party['Q.S.'] < party['P.Q.']:
                    print("PQ would have won with QS's %d votes by %d votes in %s"
                          % (party['P.Q.'], total_qs_pq - winner,
                             data['nomCirconscription']))
                    lost_pq.append(data['numeroCirconscription'])
                    lost_pq_txt.append(data['nomCirconscription'])
                if party['Q.S.'] == party['P.Q.']:
                    print("PQ/QS would have won with the others votes by %d votes in %s"
                          % (total_qs_pq - winner, data['nomCirconscription']))
                    ties.append(data['numeroCirconscription'])
                    ties_txt.append(data['nomCirconscription'])

print("Contentious counties")
print("====================")
print()

for county in lost_pq + lost_qs:
    print(counties[county]['nomCirconscription'])
    print('-' * len(counties[county]['nomCirconscription']))
    print()
    for candidate in counties[county]['candidats']:
        print(candidate['abreviationPartiPolitique'],
              candidate['nbVoteTotal'],
              candidate['tauxVote'],
              candidate['nbVoteAvance'])
    print()

print("Analysis")
print("========")
print()
print("if all QS votes went to PQ, they would have won %s"
      % ", ".join(sorted(lost_pq_txt)))
print("PQ lost %d counties because of QS" % len(lost_pq))
print("if all PQ votes went to QS, they would have won %s"
      % ", ".join(sorted(lost_qs_txt)))
print("QS lost %d counties because of PQ" % len(lost_qs))
print("%d ties found" % len(ties))

