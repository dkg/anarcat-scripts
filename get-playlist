#!/usr/bin/python3
# coding: utf-8

'''Use git-annex to get a list of files from a playlist.'''
# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
import fileinput
import logging
import subprocess

parser = argparse.ArgumentParser(description=__doc__,
                                 epilog='''This will (1) read the specified playlists, (2) remove the given
                                 PREFIX from every line, (3) git-annex
                                 get each file in the current
                                 directory.''')
parser.add_argument('--prefix', '-p',
                    help='''number or string of prefixes to remove.  if PREFIX is a number,
                    remove that many slashes from the path (default:
                    stdin)''')
parser.add_argument('--verbose', '-v', dest='log_level',
                    action='store_const', const='info', default='warning')
parser.add_argument('--debug', '-d', dest='log_level',
                    action='store_const', const='debug', default='warning')
parser.add_argument('--error', '-e', action='store_true',
                    help='treat git-annex errors as fatal and stop')
parser.add_argument('playlists', nargs='*',
                    help='''playlists to get from git-annex.''')
args = parser.parse_args()

logging.basicConfig(format='%(message)s', level=args.log_level.upper())

playlist = fileinput.input(args.playlists)

def git_annex_cmd(*args, **kwargs):
    cmd = ['git']
    if 'gitflags' in kwargs:
        cmd += kwargs['gitflags']
    cmd += ['annex']
    # completely silence git-annex unless in verbose
    if not logging.getLogger('').isEnabledFor(logging.INFO):
        cmd += [ '--quiet' ]    
    cmd += args
    logging.debug('built command %s', cmd)
    return cmd

# (1) read the specified playlist
for line in playlist:
    song = line = line.rstrip()
    if args.prefix:
        # (2) remove prefix
        try:
            args.prefix = int(args.prefix)
        except ValueError:
            # remove string prefix
            song = song[song.startswith(args.prefix) and len(args.prefix):]
        else:
            # remove int prefix
            song = song.split('/', args.prefix)[-1]
    # (3) git annex get
    if subprocess.call(git_annex_cmd('get', song, gitflags=['-c', 'annex.alwayscommit=false'])):
        if args.error:
            logging.warning('git annex failed, aborting')
            break
        else:
            logging.warning('failed to get %s', line)
    else:
        # tell user progress, because git-annex is silent if the file
        # is already present
        logging.info(song)

if subprocess.call(git_annex_cmd('merge')):
    logging.warning('git annex failed to merge')
