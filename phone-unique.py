#!/usr/bin/python

import os

import ttystatus

ts = ttystatus.TerminalStatus(period=0.5)

ts.clear()
ts.add(ttystatus.Literal('colls: '))
ts.add(ttystatus.Integer('collision'))
ts.add(ttystatus.Literal('/'))
ts.add(ttystatus.Integer('line'))
ts.add(ttystatus.Literal(' ('))
ts.add(ttystatus.PercentDone('collision', 'line', decimals=4))
ts.add(ttystatus.Literal(') '))
ts.add(ttystatus.ElapsedTime())
ts.add(ttystatus.Literal(' '))
ts.add(ttystatus.PercentDone('done', 'total', decimals=2))
ts.add(ttystatus.Literal(' '))
ts.add(ttystatus.RemainingTime('done', 'total'))
ts.add(ttystatus.Literal(' '))
ts.add(ttystatus.ByteSpeed('done'))
ts.add(ttystatus.Literal(' '))
ts.add(ttystatus.ProgressBar('done', 'total'))

s = os.stat('hashes')

ts['done'] = ts['collision'] = ts['line'] = 0
ts['total'] = s.st_size

c = {}

with open('hashes') as data:
    for line in data:
        ts['line'] += 1
        ts['done'] += len(line)
        _, h = line.split()
        if h in c:
            ts['collision'] += 1
            c[h] += 1
        else:
            c[h] = 1

ts.finish()
