#!/bin/sh

# This is a replacement for network operations in GnuPG, partially
# replacing what dirmngr does in the background. I had one too many
# obscure errors from GnuPG and figured I could just talk with the
# keyservers directly over HTTP, since that's is fundamentally how HKP
# works.
#
# Those are the features present in dirmngr not implemented here, at
# the time of writing:
#
# * LDAP
# * WKD
# * CRL and OCSP verification for X.509
# * SRV records lookup
# * auto key discovery mechanisms
# * pool health tracking
# * builtin Tor support (use torsocks)
#
# Those will not be implemented as this is not a full dirmngr
# rewrite. Think of it as a dirmngr-lite that also happens to work,
# sometimes.

set -e

KEYSERVER="https://hkps.pool.sks-keyservers.net"
CERT=/usr/share/gnupg/sks-keyservers.netCA.pem
ARGS="--cacert ${CERT} --capath /etc/ssl/certs/ --show-error --silent --location"

# get or search, defined in the HKP specification
OP=${0##*-}

for i; do
    case "$1" in
        -k|--keyserver)
            KEYSERVER=$2
            shift
            shift
            ;;
        -v|--verbose)
            set -x
            ARGS="${ARGS} --verbose"
            shift
            ;;
        -*)
            case "$OP" in
                get)
                    echo "Usage: $0 [ --keyserver keyserver ] [ --verbose ] [ fingerprint ] [ | gpg --import ]"
                    ;;
                put)
                    echo "Usage: [ gpg --armor --export fingerprint | ] $0 [ --keyserver keyserver ] [ --verbose ]"
                    ;;
                search)
                    echo "Usage: $0 [ --keyserver keyserver ] [ --verbose ] [ pattern ] [ | column -s: -t ]"
                    ;;
            esac
            exit 1
            ;;
        *)
            if [ -z "$1" ] ; then
                break
            fi
            PATTERN="$1"
            shift
            ;;
    esac
done

case "$OP" in
    get|search)
        URL="${KEYSERVER}/pks/lookup?op=${OP}&options=mr&fingerprint=on&exact=on&search=${PATTERN}"
        ;;
    put)
        URL="${KEYSERVER}/pks/add"
        ARGS="${ARGS} --data-urlencode keytext@-"
        ;;
esac

parse() {
    case "$OP" in
        search|put)
            html2text -
        ;;
        get)
            sed -n '/^-----BEGIN/,/^-----END/p'
        ;;
    esac
}

curl ${ARGS} "${URL}" | parse
