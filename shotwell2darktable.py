#! /usr/bin/python

import os
import sqlite3
import argparse

parser = argparse.ArgumentParser(description='Add missing metadata to Darktable from Shotwell.')
parser.add_argument('--dryrun', '-n', action='store_true')
parser.add_argument('--verbose', '-v', action='store_true')
parser.add_argument('--debug', '-d', action='store_true')
parser.add_argument('--limit', '-l', type=int, default=10)

args = parser.parse_args()
if args.dryrun:
    print "not doing any changes"

def maybe_print(msg):
    if args.verbose:
        print msg,

print 'loading shotwell database...'
shotwell = sqlite3.connect(os.getenv('HOME', '.') + '/.local/share/shotwell/data/photo.db')
shotwell.row_factory = sqlite3.Row

print 'loading darktable database...'
darktable = sqlite3.connect(os.getenv('HOME', '.') + '/.config/darktable/library.db')
darktable.row_factory = sqlite3.Row

print 'finding images without metadata...'
# finding images without tags, excluding darktable-specific ones or the ones i made in the import from shotwell
missing = darktable.execute('''
SELECT i.id, r.folder || "/" || i.filename AS path, r.folder, i.filename FROM images i
    LEFT JOIN tagged_images ti ON ti.imgid = i.id
        AND ti.tagid NOT IN 
            (SELECT id FROM tags 
                WHERE name LIKE 'darktable%' OR 'shotwelll%')
    LEFT JOIN film_rolls r ON r.id = i.film_id
    WHERE ti.imgid IS NULL
    ORDER BY path ASC
''')

changed = tags_found = found = total = 0

for image in missing:
    total += 1
    
    c = shotwell.execute('SELECT id, rating, filename FROM PhotoTable p  WHERE p.filename = ?', (image['path'], )).fetchone()
    if c is None:
        args.debug and maybe_print("thumb0000000000000000\tNaN\tNaN\t\t%s\n" % image['path'])
    else:
        found += 1
        report = "thumb%0.16x\t" % c['id']
        report += "%d\t%d\t" % ( c['id'], c['rating'] )

        # Shotwell (from some versions?) stores the photo identifiers
        # in a really strange way: it's the thumbnail name, which is
        # the hex version of the photo id prefixed with a bunch of
        # zeros
        #
        # example: 4998 => thumb0000000000001386
        tags = [t['name'].split('/')[-1] for t in [tag for tag in shotwell.execute('SELECT name FROM TagTable WHERE photo_id_list LIKE ?', ('%%thumb%0.16x%%' % c['id'], ))]]
        report += ",".join(tags)
        report += "\t%s" % image['path']
        if args.debug or len(tags) > 0:
            print report
        if len(tags) > 0:
            tags_found += 1
            args.dryrun and maybe_print("would be")
            query = "UPDATE images SET flags = ( (flags&4088) + ? ) WHERE id = ?"
            maybe_print("executing " + query.replace('?', '%d') % (c['rating'], image['id']) + "\n")
            if not args.dryrun:
                assert darktable.execute(query, (c['rating'], image['id'])).rowcount == 1
                changed += 1
            for tag in tags:
                args.dryrun and maybe_print("would be")
                query = 'INSERT INTO tagged_images SELECT ?, id FROM tags WHERE name = ?'
                maybe_print("executing " + query.replace('?', '%s') % (str(image['id']), tag) + "\n")
                if not args.dryrun:
                    assert darktable.execute(query, (image['id'], tag)).rowcount == 1

            # add tags on the file itself
            args.dryrun and maybe_print("would be")
            command = u'''exiv2 -eaX  -M 'add Xmp.dc.subject XmpBag "%s"' %s''' % (u", ".join(tags), image['path'])
            maybe_print(u"executing %s\n" % command)
            good_sidecar = image['path'] + '.xmp'
            bad_sidecar = image['path'][:-4] + '.xmp'
            args.dryrun and maybe_print("would be")
            maybe_print("renaming %s to %s\n" % (bad_sidecar, good_sidecar))
            if not args.dryrun:
                ret = os.system(command.encode('utf-8'))
                if ret != 0:
                    print "warning: exiv command return %d, ran: %s" % (ret, command)
                else:
                    os.rename(bad_sidecar, good_sidecar)
            if tags_found >= args.limit:
                break

if not args.dryrun:
    darktable.commit()

print "%d/%d images found, %d found with metadata, %d changed" % (found, total, tags_found, changed)
