#!/usr/bin/python

'''create a rainbow table of phone number hashes

this more or less follows the hashing algorithm used by the Signal
messaging software to hide phone numbers from the server while still
allowing contact discovery. this was implemented to contribute to the
conversation regarding concerns about phone number sharing in Signal
outlined here:

https://www.jwz.org/blog/2017/03/signal-leaks-your-phone-number-to-everyone-in-your-contacts/

This is the comment I have posted there, which is relevant to
understand the output here:

As far as I can tell, the phone number hashing happens in the signal
library, in
[SignalServiceAccountManager.createDirectoryServerToken()][1]. This
is used, for example, by the getContact() function to check if a phone
number is on Signal.  Here is the relevant part of the implementation:

MessageDigest digest = MessageDigest.getInstance("SHA1");
byte[] token = Util.trim(digest.digest(e164number.getBytes()), 10);
String encoded = Base64.encodeBytesWithoutPadding(token);

 [1]: https://github.com/WhisperSystems/libsignal-service-java/blob/a722aef284e2ce08bfc63f4e15be3fbaa44e4651/java/src/main/java/org/whispersystems/signalservice/api/SignalServiceAccountManager.java#L296

The problem is of course that this means it's fairly trivial (say a
compromised signal server) for an attacker to create a rainbow table
of the possible phone numbers in the region, country or world,
depending on the available resources. This, then, makes it possible
for an attacker to actually guess the phone numbers from those hashes.

I've made some experiments: a rainbow table for all of north america
would take less than 3 days to generate on this machine (Intel
i3-6100U, using only one core). The resulting file would be around
460GB. After generating about 0.23% of the table (about 4M phone
numbers), about 800 collisions were found in the first 10 digits of
the hashes, which is a 0.002% collision rate. This doesn't say much of
course, but it would still make the rainbow table very efficient. I
haven't evaluated how probable are collisions with the given prefix at
the theoretical level.

That said, I haven't seen anything in the Signal source code that
would confirm (or deny) that Signal would share previously unknown
phone numbers between users. What is sent is a hashed version and the
two reports from Twitter users saying that phone numbers were linked
were addressed by OWS staff: in both cases, there were other possible
explanations, mostly concerning third party contact sharing services
like Google+ and other storage mechanisms.

The Signal source code is pretty well laid out and was audited by
third-party firms in the past, at least twice. While both reviews
focused on cryptographic primitives, the latter did review the whole
Signal library, which includes the above code and did not find
significant issues. I would therefore conclude that the "phone number
leak" can be explained by some other entity than Signal. I am not
saying it did not happen, from a user perspective, but that people
were simply not aware that their phone numbers were already leaked
through some other mechanism by another third party than Signal, which
we can hardly blame OWS for.

It is true that contact discovery, the way it is implemented by
signal, favors more the development of the network than the privacy of
the users. But it's a double-edged sword: either you make something
"super-secure" that never reaches critical mass and sees limited
adoption because it's too hard to use and discover other users
(e.g. OpenPGP, Pond, XMPP and plenty of others) or you compromise on
certain security properties and try to reach more massive adoption.

I think the latter is a laudable goal: for too long crypto-geeks have
focused on making tools for themselves and ignoring the vast majority
of the population. It's about time someone fights for the users...
'''

# to count the number of hash collisions, I have used the following
# fancy pipeline:
#
# terminalA$ time ./phone-hash.py > hashes
# terminalB$ wc=$(wc -l hashes | cut -f 1 -d' ') ; cut -d'        'f2 hashes | pv -c -N cut -l -s $wc | sort | pv -c -N sort1 -l -s $wc | uniq -c | pv -c -l -s $wc -N uniq | grep -v ' 1 ' | pv -c -l -N grep | wc -l
#
# to estimate the final rainbow table file size, I have looked at the
# estimated remaining time from the program and the current file size
# and made a simple "règle de trois":
#
# $ qalc
# > (((1min + 11s)/(62h + 58 min + 42s) = (144113664bytes)/(x bytes)) 
#
#   (((1 * minute) + (11 * second)) / ((62 * hour) + (58 * minute) + (42 * second))) = ((1.4411366E8 * byte) / (x * byte)) = approx. x = 4.6019349E11
#
# > 4.6019349E11 bytes
#
#   4.6019349 * (10^11) * byte = 460.19349 gigabytes

# this is obviously very slow. it could be optimized
# significantly. the resulting output is probably too badly formatted
# to be useable in any meaningful way, which is fine because we just
# want stats out of this. a real proof of concept would use a more
# efficient storage than what is basically CSV here, obviously.

# this is not implemented to prevent script kiddie abuse, although how
# such kids would get their hands on the hashes in the first place
# makes that questionnable...

import hashlib

import ttystatus

digest = hashlib.sha1()

ts = ttystatus.TerminalStatus(period=0.5)

ts.clear()
ts.add(ttystatus.ElapsedTime())
ts.add(ttystatus.Literal(' '))
ts.add(ttystatus.PercentDone('done', 'total', decimals=2))
ts.add(ttystatus.Literal(' '))
ts.add(ttystatus.RemainingTime('done', 'total'))
ts.add(ttystatus.Literal(' '))
ts.add(ttystatus.ByteSpeed('done'))
ts.add(ttystatus.Literal(' '))
ts.add(ttystatus.ProgressBar('done', 'total'))
ts['done'] = 0
ts['total'] = 19999999999-10000000

# range of phone numbers to test
#
# this is +1-100-100-1000
start = 11001000000
# this is +1-999-999-9999
end = 19999999999
# obviously there are invalid phone numbers we could skip in there. in
# particular, we could restrict ourselves to valid area codes, for
# example

for num in xrange(start, end):
    digest = hashlib.sha1(bytes(num)).hexdigest()[:10]
    ts['done'] += 1
    print("%d\t%s" % (num, digest))

ts.finish()
